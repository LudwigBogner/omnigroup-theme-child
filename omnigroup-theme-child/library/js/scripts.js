/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO VARIABLES OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
// Global variables
mob;

// Browser prefixes
nativeTransition =
  'Transition' in document.body.style && 'transition' ||  
  'WebkitTransition' in document.body.style && '-webkit-transition' ||
  'MozTransition' in document.body.style && '-moz-transition' ||
  'msTransition' in document.body.style && '-ms-transition' ||
  'OTransition' in document.body.style && '-o-transition';
  
nativeTransform =
  'Transform' in document.body.style && 'transform' ||  
  'WebkitTransform' in document.body.style && '-webkit-transform' ||
  'MozTransform' in document.body.style && '-moz-transform' ||
  'msTransform' in document.body.style && '-ms-transform' ||
  'OTransform' in document.body.style && '-o-transform';
  
nativeTransitionEnd =
  'Transition' in document.body.style && 'transitionend' || 
  'WebkitTransition' in document.body.style && 'webkitTransitionEnd' ||
  'MozTransition' in document.body.style && 'transitionend' ||
  'OTransition' in document.body.style && 'oTransitionEnd';
  
/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO VIEWPORT DIMENSIONS OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
function updateViewportDimensions() {
  var w=window,d=document,e=d.documentElement,g=d.getElementsByTagName('body')[0],x=w.innerWidth||e.clientWidth||g.clientWidth,y=w.innerHeight||e.clientHeight||g.clientHeight;
  return { width:x,height:y };
}
// setting the viewport width
var viewport = updateViewportDimensions();

/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO THROTTLE RESIZE OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
    if (timers[uniqueId]) { clearTimeout (timers[uniqueId]); }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = $('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * $(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *  // update the viewport, in case the window size has changed
 *  viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO DOCUMENT READY OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
jQuery(document).ready(function($) {
  /* MOBILE CHECK - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
  function checkMobile() {
    oldM = mob;
    if ($('body').css('font-size') == '25px'){
      mob = false;
    }
    if ($('body').css('font-size') !== '25px'){
      mob = true;
    }

    if (oldM !== undefined) {
      if (oldM != mob) {
        if (mob == true) {
          // Do mobile changes
        } else {

        }
      }
    }
  }

  checkMobile();
});