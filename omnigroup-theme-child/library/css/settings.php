<?php
// This file can be used to access and output options from the WordPress admin that effect CSS rules.
// For example if you create a page "Options" and add some custom fields with choices of colour or size, you can access them here and output the values.

$absolute_path = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
$wp_load = $absolute_path[0] . 'wp-load.php';
require_once($wp_load);

// Set your PHP variables here
// This example would get the field "main_colour" from the post or page with the id of 5: $mainColour = get_field('main_colour', 5);
	
// Output the file as a CSS file
header('Content-type: text/css');
header('Cache-control: must-revalidate');	
?>
<?php // You call the php variables you set above with a combination of normal CSS and PHP tags: ?>
.example {
	background-color: <?php echo $mainColour;?>;  
}