<?php
function hide_parent_theme($themes) {
  unset($themes['omnigroup']);
  return $themes;
}
add_filter('wp_prepare_themes_for_js','hide_parent_theme',11,1);
/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO THEME SUPPORT OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
// Adding functions & Theme Support
function theme_theme_support() {
  add_theme_support('post-thumbnails');
}

/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO THEME SCRIPTS AND STYLES OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
function theme_scripts_and_styles() {
  global $wp_styles;
  if (!is_admin()) {
    /* REGISTER SCRIPTS AND STYLES - - - - - - - - - - - - - - - - - - - - - - - - - - - - */    
    // These scripts are all included in the parent theme
    wp_register_script( 'wait', get_template_directory() . '/library/js/libs/wait.min.js', array(), '', true );    
  
    // Register scripts for this website, place them in library/js/libs/
    wp_register_script( 'unique-handle', get_stylesheet_directory_uri() . '/library/js/libs/filename.js', array(), '', true ); 
                     
    // Register stylesheets
    wp_register_style( 'fonts', get_stylesheet_directory_uri() . '/library/css/fonts/fonts.css', array(), '', 'all' );
    wp_register_style( 'settings', get_stylesheet_directory_uri() . '/library/css/settings.php', array(), '', 'all' );

    /* ENQUEUE SCRIPTS AND STYLES - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
    // Enqueue included scripts
    //wp_enqueue_script( 'wait' );
    
    // Enqueue added scripts, copy and paste and change your unique-handle
    //wp_enqueue_script( 'unique-handle' );    

    // Enqueue styles
    //wp_enqueue_style( 'settings' ); // Check the settings.php file for instructions on how to use it.
    wp_enqueue_style( 'fonts' );    
  }
}

/* OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO THEME FUNCTIONS OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO */
// Remove menu items
function remove_menus() {   
  //add_menu_page( 'information', 'Information ', 'edit_posts', 'post.php?post=5&action=edit', '', '', 9 ); // Example of how to add a menu item
  remove_menu_page('edit-comments.php'); // Comments
  remove_menu_page('plugins.php'); //Plugins
  remove_menu_page('upload.php'); //Media
  remove_menu_page('edit.php'); //Posts 
  remove_menu_page('tools.php'); //Tools
  remove_menu_page('edit.php?post_type=page'); //Pages  
  remove_menu_page('options-general.php'); //Settings
  remove_menu_page('themes.php'); //Appearance
}

// If the user is not the first user created (i.e you, becuase the first user always has an id of 1)
if (wp_get_current_user()->ID != 1) {
  add_action('admin_menu', 'remove_menus');
}
?>